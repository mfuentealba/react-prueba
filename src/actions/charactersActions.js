import { SEND_LIST_CHARACTERS, RECEIVE_LIST_CHARACTERS, REQUEST_CHARACTER, RECEIVE_CHARACTER } from "../constants/characters.constants"

export const getAllCharactersAction = (payload) => {
    return { type: SEND_LIST_CHARACTERS, payload }
}

export const receiveAllCharactersAction = (payload) => {
    return { type: RECEIVE_LIST_CHARACTERS, payload }
}


export const requestCharacterAction = (payload) => {
    return { type: REQUEST_CHARACTER, payload }
}

export const receiveCharacterAction = (payload) => {
    return { type: RECEIVE_CHARACTER, payload }
}