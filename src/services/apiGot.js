
import axios from 'axios';



export const apiGOT = async (value) => {
    
    try{
        let url = `${process.env.REACT_APP_API_URL}${value.payload.id ? value.payload.id : ''}`;
        
        return await axios.get(url, {params: {page: value.payload.page, opt: value.payload.opt, value: value.payload.value}});
        
        
    } catch(e){ 
        return e
    }
}



