const charactersSelector = {
    listCharacters: state => state.charactersReducer.listCharacters,
    activeCharacter: state => state.charactersReducer.activeCharacter,
    activeLoading: state => state.charactersReducer.activeLoading,
    pages: state => state.charactersReducer.pages,
}

export { charactersSelector }