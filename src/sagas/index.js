import { takeEvery, call, put, all, cancelled, take, fork, cancel, takeLatest } from "redux-saga/effects";

import { SEND_LIST_CHARACTERS, RECEIVE_LIST_CHARACTERS, REQUEST_CHARACTER } from "../constants/characters.constants";
import { apiGOT } from "../services/apiGot";
import { receiveAllCharactersAction, receiveCharacterAction } from "../actions/charactersActions";



function* getAllCharacters(values) {


    let { data } = yield call(apiGOT, values);
    yield put(receiveAllCharactersAction(data));

}


function* getCharacter(values) {


    let { data } = yield call(apiGOT, values);
    yield put(receiveCharacterAction(data));

}

export default function* fnProcesoSaga() {
    yield takeLatest(SEND_LIST_CHARACTERS, getAllCharacters)
    yield takeLatest(REQUEST_CHARACTER, getCharacter)
    console.log("desde saga")

}


