import { combineReducers } from 'redux';

import { charactersReducer } from './characters.reducer';

const reducers = combineReducers({
    charactersReducer
});

export { reducers }