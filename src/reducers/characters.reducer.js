import Immutable from "immutable";
import { SEND_LIST_CHARACTERS, RECEIVE_LIST_CHARACTERS, REQUEST_CHARACTER, RECEIVE_CHARACTER } from "../constants/characters.constants";

const INITIAL_STATE = {
    listCharacters: Immutable.List(),
    activeCharacter: {},
    activeLoading: false, 
    pages: 0
}

const charactersReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case SEND_LIST_CHARACTERS:
            return {
                ...state,
                activeLoading: true,
                listCharacters: Immutable.List()
            }
        case RECEIVE_LIST_CHARACTERS:
           /* let listado = action.payload.map(function(item) {
                return x * 2;
             });*/
            return {
                ...state,
                listCharacters: Immutable.List(action.payload.docs)
                , activeLoading: false
                , pages: action.payload.pages

            }
        case REQUEST_CHARACTER:
            return {
                ...state, 
                activeCharacter: {}, activeLoading: true
            }
    
            case RECEIVE_CHARACTER:
                return {
                    ...state, 
                    activeCharacter: action.payload, activeLoading: false
                }

        default:
            return state
    }
}

export { charactersReducer }