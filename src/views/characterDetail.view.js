import React, { Component } from 'react';
import { connect } from 'react-redux';
import { charactersSelector } from '../selectors/characters.selector';
import { requestCharacterAction } from '../actions/charactersActions';
import { Paper, Button } from '@material-ui/core';

 class CharacterDetailView extends Component {
    constructor(props) { 
        super(props);
        
        
        //this.getCharacter = this.getCharacter.bind(this);
    }

    componentDidMount(){
        const { match: { params } } = this.props;
        this.props.getCharacter(params.id)
    }

    render () {
        const { activeCharacter } = this.props;
        return (
           <Paper>
               {
                   activeCharacter ? <img src={activeCharacter.image ? activeCharacter.image : ''}/> : null
               }
               <p>
               Nombre: {activeCharacter.name ? activeCharacter.name : 'Nn'} <br /> 
               Sexo: {activeCharacter.gender ? activeCharacter.gender : 'No indica'}<br /> 
               Slug: {activeCharacter.slug ? activeCharacter.slug : 'No indica'}<br /> 
               Rank:{activeCharacter.pagerank ? activeCharacter.pagerank.rank : 'No indica'}<br /> 
               House:{activeCharacter.house ? activeCharacter.house : 'No indica'}<br /> <br /><br />
               Libros:{activeCharacter.books ? activeCharacter.books.join(', ') : 'No Figura'}<br /> 
               Titulos:{activeCharacter.titles ? activeCharacter.titles.join(', ') : 'No Figura'}<br /> 
               </p>
               <Button variant="contained" color="primary" onClick={() => { this.props.history.push('/')}}>
                    Volver
                </Button>
           </Paper>
        );
    }
}

function mapState(state) {
    const {activeCharacter } = state.charactersReducer;
    return { activeCharacter } ;
}

const mapDispatchToProps = dispatch => {
    
    return {
        getCharacter: (id) => dispatch(requestCharacterAction({id}))
      }
};


export default connect(mapState, mapDispatchToProps)(CharacterDetailView);

