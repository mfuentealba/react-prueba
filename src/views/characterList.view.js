import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import Pagination, { usePagination } from '@material-ui/lab/Pagination';
import { List, makeStyles, ListItem, ListItemAvatar, ListItemText, Typography, Avatar, Divider, ListItemSecondaryAction, IconButton, Container, Paper, Radio, TextField, FormControl, FormControlLabel, RadioGroup, Button } from "@material-ui/core";
import Check from '@material-ui/icons/Check';
import { charactersSelector } from "../selectors/characters.selector";
import { getAllCharactersAction } from "../actions/charactersActions";
import { history } from "../store";


const useStyles = makeStyles((theme) => ({
    root: {

        height: "80%",
        backgroundColor: theme.palette.background.paper,
        maxHeight: '100%',
        overflowY: 'scroll'
    },
    inline: {
        display: 'inline',
    },
    img: {
        height: "80px",
        width: "60px"
    }
}));


const CharacterListView = (props) => {


    const pages = useSelector(charactersSelector.pages);
    let listCharacters = useSelector(charactersSelector.listCharacters);
    listCharacters = listCharacters.valueSeq().toArray();


    const [page, setPage] = React.useState(1);
    const [optSearch, setOptSearch] = React.useState('name');
    const [text, setText] = React.useState('');
    const dispatch = useDispatch();
    const classes = useStyles();


    const handleChange = (event, value) => {
        setPage(value);
        dispatch(getAllCharactersAction({ page: value, opt: optSearch, value: text }));
    };

    useEffect(() => {
        dispatch(getAllCharactersAction({ page: page }));
    }, [])

    const handleChangeSearch = (event) => {
        setOptSearch(event.target.value);
    };

    const search = (event) => {
        dispatch(getAllCharactersAction({ page: page, opt: optSearch, value: text }));
    };

    const handleText = (event) => {
        const { value } = event.target;


        setText(value);

    };

    return (
        <Container>

            <Paper>
                <TextField id="outlined-basic" label={optSearch} variant="outlined" onBlur={handleText} />
                <Button variant="contained" color="primary" onClick={search}>
                    Buscar
                </Button>
                <RadioGroup row aria-label="position" name="position" defaultValue="top">
                    <FormControlLabel
                        
                        onChange={handleChangeSearch}
                        value="name"
                        name="radio-button-demo"
                        
                        label="Nombre"
                        control={<Radio color="primary" checked={optSearch === 'name'}/>}
                    />
                    <FormControlLabel
                        checked={optSearch === 'house'}
                        onChange={handleChangeSearch}
                        value="house"
                        name="radio-button-demo"
                        
                        label="Casa"
                        control={<Radio color="primary" />}
                    />
                </RadioGroup>



            </Paper>
            <List className={classes.root}>
                {
                    listCharacters.map((item, key) => {
                        console.log(item)

                        const detailCharacter = () => {
                            //dispatch(getAllCharactersAction({ page: page, id: item.id }));
                            history.push('/detail/' + item.id)
                        }





                        return (
                            <React.Fragment key={item.id}>
                                <ListItem>
                                    <ListItemAvatar>

                                        {
                                            item.image ?
                                                <img className={classes.img} src={item.image} /> : <Avatar></Avatar>
                                        }


                                    </ListItemAvatar>
                                    <ListItemText
                                        primary={item.name}
                                        secondary={
                                            item.house
                                        }
                                    />
                                    <ListItemSecondaryAction>

                                        <IconButton edge="end" aria-label="delete" onClick={detailCharacter}>
                                            <Check></Check>
                                        </IconButton>




                                    </ListItemSecondaryAction>
                                </ListItem>
                                <Divider variant="inset" component="li" />
                            </React.Fragment>
                        )
                    })
                }

            </List >
            <Pagination count={pages} page={page} onChange={handleChange} />
        </Container>

    )


}

export default CharacterListView