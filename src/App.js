import React, { useEffect } from 'react';

import './App.css';
import { makeStyles, Box, Button } from '@material-ui/core';
import { Router, Route, Switch, Redirect } from 'react-router';
import { grey, teal } from '@material-ui/core/colors';
import { useDispatch, useSelector } from 'react-redux';

import NotFoundView from './views/not.found.view';
import CharacterListView from './views/characterList.view';
import { charactersSelector } from './selectors/characters.selector';
import { getAllCharactersAction } from './actions/charactersActions';
import CharacterDetailView from './views/characterDetail.view';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    width: (window.innerWidth - 0),//"100%",
    height: (window.innerHeight - 90),// document.body.clientHeight,
    marginTop: "0px",
    marginBottom: "0px",
    alignItems: "center",
    justifyContent: "center",
    scrollBehavior: "auto"



  },
  panel: {
    width: "100%",
    height: "100%"
  },
  barra: {
    padding: "10px",
    backgroundColor: teal[500],
    margin: "0px",
    height: "15px",
    boxSizing: "content-box"

  },
  barra2: {
    padding: "10px",
    backgroundColor: grey[800],
    margin: "0px",
    height: "30px",
    boxSizing: "content-box"


  },
  btnLogin: {
    color: "#FFFFFF",


  }
}));



const App = (props) => {
  const activeLoading = useSelector(charactersSelector.activeLoading)
  
  const dispatch = useDispatch()

  const classes = useStyles();
  
  

  return (
    <Router history={props.history}>
      <Switch>
        <Route path='/' exact component={CharacterListView} />
        <Route path='/detail/:id' component={CharacterDetailView} />
        <Route path='*' component={NotFoundView} />
      </Switch>
    </Router>
  );
}

export default App;
