import { createStore, applyMiddleware } from 'redux';
import { reducers } from './reducers';
import createSagaMiddleware from 'redux-saga';
import  {createBrowserHistory}  from 'history';


import fnProcesoSaga from "./sagas";


const history = createBrowserHistory();
const sagaMiddleware = createSagaMiddleware();
const store = createStore(reducers, applyMiddleware(sagaMiddleware));
sagaMiddleware.run(fnProcesoSaga);
export { store, history }
