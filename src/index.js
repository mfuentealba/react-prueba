import React from 'react'
import ReactDOM from 'react-dom'
import { history } from "./store";
import App from './App'
import { store } from './store'
import { Provider } from 'react-redux'
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
  <Provider store={store}>
    <App history={history} />
  </Provider>,


  document.getElementById('root'))

serviceWorker.unregister();